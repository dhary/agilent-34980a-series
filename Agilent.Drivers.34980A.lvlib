﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Item Name="AG34980A Abort Counter Measurement.vi" Type="VI" URL="../Public/AG34980A Abort Counter Measurement.vi"/>
		<Item Name="AG34980A Abort Digital Output Memory.vi" Type="VI" URL="../Public/AG34980A Abort Digital Output Memory.vi"/>
		<Item Name="AG34980A Abort.vi" Type="VI" URL="../Public/AG34980A Abort.vi"/>
		<Item Name="AG34980A Clear Alarm.vi" Type="VI" URL="../Public/AG34980A Clear Alarm.vi"/>
		<Item Name="AG34980A Clear Digital Input Memory.vi" Type="VI" URL="../Public/AG34980A Clear Digital Input Memory.vi"/>
		<Item Name="AG34980A Clear Totalizer.vi" Type="VI" URL="../Public/AG34980A Clear Totalizer.vi"/>
		<Item Name="AG34980A Close.vi" Type="VI" URL="../Public/AG34980A Close.vi"/>
		<Item Name="AG34980A Configure 4-Wire Resistance.vi" Type="VI" URL="../Public/AG34980A Configure 4-Wire Resistance.vi"/>
		<Item Name="AG34980A Configure AC Current.vi" Type="VI" URL="../Public/AG34980A Configure AC Current.vi"/>
		<Item Name="AG34980A Configure AC Voltage.vi" Type="VI" URL="../Public/AG34980A Configure AC Voltage.vi"/>
		<Item Name="AG34980A Configure Alarm.vi" Type="VI" URL="../Public/AG34980A Configure Alarm.vi"/>
		<Item Name="AG34980A Configure Channel Delay.vi" Type="VI" URL="../Public/AG34980A Configure Channel Delay.vi"/>
		<Item Name="AG34980A Configure Counter.vi" Type="VI" URL="../Public/AG34980A Configure Counter.vi"/>
		<Item Name="AG34980A Configure Data Format.vi" Type="VI" URL="../Public/AG34980A Configure Data Format.vi"/>
		<Item Name="AG34980A Configure DC Current.vi" Type="VI" URL="../Public/AG34980A Configure DC Current.vi"/>
		<Item Name="AG34980A Configure DC Voltage.vi" Type="VI" URL="../Public/AG34980A Configure DC Voltage.vi"/>
		<Item Name="AG34980A Configure Digital Alarm.vi" Type="VI" URL="../Public/AG34980A Configure Digital Alarm.vi"/>
		<Item Name="AG34980A Configure Digital Handshake.vi" Type="VI" URL="../Public/AG34980A Configure Digital Handshake.vi"/>
		<Item Name="AG34980A Configure Digital Input Memory Interrupt.vi" Type="VI" URL="../Public/AG34980A Configure Digital Input Memory Interrupt.vi"/>
		<Item Name="AG34980A Configure Digital Input.vi" Type="VI" URL="../Public/AG34980A Configure Digital Input.vi"/>
		<Item Name="AG34980A Configure Digital Output Memory Interrupt.vi" Type="VI" URL="../Public/AG34980A Configure Digital Output Memory Interrupt.vi"/>
		<Item Name="AG34980A Configure Digital Output Memory.vi" Type="VI" URL="../Public/AG34980A Configure Digital Output Memory.vi"/>
		<Item Name="AG34980A Configure Digital Output.vi" Type="VI" URL="../Public/AG34980A Configure Digital Output.vi"/>
		<Item Name="AG34980A Configure Digital.vi" Type="VI" URL="../Public/AG34980A Configure Digital.vi"/>
		<Item Name="AG34980A Configure Frequency and Period.vi" Type="VI" URL="../Public/AG34980A Configure Frequency and Period.vi"/>
		<Item Name="AG34980A Configure FRTD.vi" Type="VI" URL="../Public/AG34980A Configure FRTD.vi"/>
		<Item Name="AG34980A Configure Integration Time.vi" Type="VI" URL="../Public/AG34980A Configure Integration Time.vi"/>
		<Item Name="AG34980A Configure Microwave Bank.vi" Type="VI" URL="../Public/AG34980A Configure Microwave Bank.vi"/>
		<Item Name="AG34980A Configure Microwave Channels.vi" Type="VI" URL="../Public/AG34980A Configure Microwave Channels.vi"/>
		<Item Name="AG34980A Configure Microwave Module.vi" Type="VI" URL="../Public/AG34980A Configure Microwave Module.vi"/>
		<Item Name="AG34980A Configure Output Clock.vi" Type="VI" URL="../Public/AG34980A Configure Output Clock.vi"/>
		<Item Name="AG34980A Configure Resistance.vi" Type="VI" URL="../Public/AG34980A Configure Resistance.vi"/>
		<Item Name="AG34980A Configure RTD.vi" Type="VI" URL="../Public/AG34980A Configure RTD.vi"/>
		<Item Name="AG34980A Configure Scale.vi" Type="VI" URL="../Public/AG34980A Configure Scale.vi"/>
		<Item Name="AG34980A Configure Scan.vi" Type="VI" URL="../Public/AG34980A Configure Scan.vi"/>
		<Item Name="AG34980A Configure Thermistor.vi" Type="VI" URL="../Public/AG34980A Configure Thermistor.vi"/>
		<Item Name="AG34980A Configure Thermocouple.vi" Type="VI" URL="../Public/AG34980A Configure Thermocouple.vi"/>
		<Item Name="AG34980A Configure Totalizer.vi" Type="VI" URL="../Public/AG34980A Configure Totalizer.vi"/>
		<Item Name="AG34980A Configure Trigger.vi" Type="VI" URL="../Public/AG34980A Configure Trigger.vi"/>
		<Item Name="AG34980A Create Custom Digital Trace.vi" Type="VI" URL="../Public/AG34980A Create Custom Digital Trace.vi"/>
		<Item Name="AG34980A Create Standard Digital Trace.vi" Type="VI" URL="../Public/AG34980A Create Standard Digital Trace.vi"/>
		<Item Name="AG34980A DAC Apply Level.vi" Type="VI" URL="../Public/AG34980A DAC Apply Level.vi"/>
		<Item Name="AG34980A DAC Catalog.vi" Type="VI" URL="../Public/AG34980A DAC Catalog.vi"/>
		<Item Name="AG34980A DAC Configure Pacing.vi" Type="VI" URL="../Public/AG34980A DAC Configure Pacing.vi"/>
		<Item Name="AG34980A DAC Configure Trace.vi" Type="VI" URL="../Public/AG34980A DAC Configure Trace.vi"/>
		<Item Name="AG34980A DAC Create Custom Trace.vi" Type="VI" URL="../Public/AG34980A DAC Create Custom Trace.vi"/>
		<Item Name="AG34980A DAC Create Standard Trace.vi" Type="VI" URL="../Public/AG34980A DAC Create Standard Trace.vi"/>
		<Item Name="AG34980A DAC Delete Trace.vi" Type="VI" URL="../Public/AG34980A DAC Delete Trace.vi"/>
		<Item Name="AG34980A DAC Free Memory.vi" Type="VI" URL="../Public/AG34980A DAC Free Memory.vi"/>
		<Item Name="AG34980A DAC Halt Trace.vi" Type="VI" URL="../Public/AG34980A DAC Halt Trace.vi"/>
		<Item Name="AG34980A DAC Output State.vi" Type="VI" URL="../Public/AG34980A DAC Output State.vi"/>
		<Item Name="AG34980A DAC Start Trace.vi" Type="VI" URL="../Public/AG34980A DAC Start Trace.vi"/>
		<Item Name="AG34980A DAC Trace Points.vi" Type="VI" URL="../Public/AG34980A DAC Trace Points.vi"/>
		<Item Name="AG34980A DAC Trigger Immediate.vi" Type="VI" URL="../Public/AG34980A DAC Trigger Immediate.vi"/>
		<Item Name="AG34980A Define Sequence.vi" Type="VI" URL="../Public/AG34980A Define Sequence.vi"/>
		<Item Name="AG34980A Delete Digital Trace.vi" Type="VI" URL="../Public/AG34980A Delete Digital Trace.vi"/>
		<Item Name="AG34980A Delete Sequence.vi" Type="VI" URL="../Public/AG34980A Delete Sequence.vi"/>
		<Item Name="AG34980A Digital Input Data Points.vi" Type="VI" URL="../Public/AG34980A Digital Input Data Points.vi"/>
		<Item Name="AG34980A Digital Input Interrupt Status.vi" Type="VI" URL="../Public/AG34980A Digital Input Interrupt Status.vi"/>
		<Item Name="AG34980A Digital Input Memory Status.vi" Type="VI" URL="../Public/AG34980A Digital Input Memory Status.vi"/>
		<Item Name="AG34980A Digital IO.vi" Type="VI" URL="../Public/AG34980A Digital IO.vi"/>
		<Item Name="AG34980A Digital Output Memory Status.vi" Type="VI" URL="../Public/AG34980A Digital Output Memory Status.vi"/>
		<Item Name="AG34980A Digital Trace Catalog.vi" Type="VI" URL="../Public/AG34980A Digital Trace Catalog.vi"/>
		<Item Name="AG34980A Digital Trace Memory Free.vi" Type="VI" URL="../Public/AG34980A Digital Trace Memory Free.vi"/>
		<Item Name="AG34980A Digital Trace Points.vi" Type="VI" URL="../Public/AG34980A Digital Trace Points.vi"/>
		<Item Name="AG34980A Display.vi" Type="VI" URL="../Public/AG34980A Display.vi"/>
		<Item Name="AG34980A DMM Cycles.vi" Type="VI" URL="../Public/AG34980A DMM Cycles.vi"/>
		<Item Name="AG34980A Error Query (Multiple).vi" Type="VI" URL="../Public/AG34980A Error Query (Multiple).vi"/>
		<Item Name="AG34980A Error Query.vi" Type="VI" URL="../Public/AG34980A Error Query.vi"/>
		<Item Name="AG34980A Fetch.vi" Type="VI" URL="../Public/AG34980A Fetch.vi"/>
		<Item Name="AG34980A Initialize.vi" Type="VI" URL="../Public/AG34980A Initialize.vi"/>
		<Item Name="AG34980A Initiate Counter Measurement.vi" Type="VI" URL="../Public/AG34980A Initiate Counter Measurement.vi"/>
		<Item Name="AG34980A Initiate.vi" Type="VI" URL="../Public/AG34980A Initiate.vi"/>
		<Item Name="AG34980A Is Channel Closed.vi" Type="VI" URL="../Public/AG34980A Is Channel Closed.vi"/>
		<Item Name="AG34980A List to 3D Array.vi" Type="VI" URL="../Public/AG34980A List to 3D Array.vi"/>
		<Item Name="AG34980A List to Array.vi" Type="VI" URL="../Public/AG34980A List to Array.vi"/>
		<Item Name="AG34980A Measure 4-Wire Resistance.vi" Type="VI" URL="../Public/AG34980A Measure 4-Wire Resistance.vi"/>
		<Item Name="AG34980A Measure AC Current.vi" Type="VI" URL="../Public/AG34980A Measure AC Current.vi"/>
		<Item Name="AG34980A Measure AC Voltage.vi" Type="VI" URL="../Public/AG34980A Measure AC Voltage.vi"/>
		<Item Name="AG34980A Measure DC Current.vi" Type="VI" URL="../Public/AG34980A Measure DC Current.vi"/>
		<Item Name="AG34980A Measure DC Voltage.vi" Type="VI" URL="../Public/AG34980A Measure DC Voltage.vi"/>
		<Item Name="AG34980A Measure Frequency and Period.vi" Type="VI" URL="../Public/AG34980A Measure Frequency and Period.vi"/>
		<Item Name="AG34980A Measure FRTD.vi" Type="VI" URL="../Public/AG34980A Measure FRTD.vi"/>
		<Item Name="AG34980A Measure Resistance.vi" Type="VI" URL="../Public/AG34980A Measure Resistance.vi"/>
		<Item Name="AG34980A Measure RTD.vi" Type="VI" URL="../Public/AG34980A Measure RTD.vi"/>
		<Item Name="AG34980A Measure Thermistor.vi" Type="VI" URL="../Public/AG34980A Measure Thermistor.vi"/>
		<Item Name="AG34980A Measure Thermocouple.vi" Type="VI" URL="../Public/AG34980A Measure Thermocouple.vi"/>
		<Item Name="AG34980A Measurement Function.vi" Type="VI" URL="../Public/AG34980A Measurement Function.vi"/>
		<Item Name="AG34980A Microwave Channel Status.vi" Type="VI" URL="../Public/AG34980A Microwave Channel Status.vi"/>
		<Item Name="AG34980A Microwave Module Status.vi" Type="VI" URL="../Public/AG34980A Microwave Module Status.vi"/>
		<Item Name="AG34980A Monitor.vi" Type="VI" URL="../Public/AG34980A Monitor.vi"/>
		<Item Name="AG34980A Query Alarm Queue.vi" Type="VI" URL="../Public/AG34980A Query Alarm Queue.vi"/>
		<Item Name="AG34980A Query External Reference Junction.vi" Type="VI" URL="../Public/AG34980A Query External Reference Junction.vi"/>
		<Item Name="AG34980A Query Fixed Reference Junction.vi" Type="VI" URL="../Public/AG34980A Query Fixed Reference Junction.vi"/>
		<Item Name="AG34980A Query Internal Reference Junction.vi" Type="VI" URL="../Public/AG34980A Query Internal Reference Junction.vi"/>
		<Item Name="AG34980A Query Last Measurement.vi" Type="VI" URL="../Public/AG34980A Query Last Measurement.vi"/>
		<Item Name="AG34980A Read All Digital Input Memory Data.vi" Type="VI" URL="../Public/AG34980A Read All Digital Input Memory Data.vi"/>
		<Item Name="AG34980A Read Counter Data.vi" Type="VI" URL="../Public/AG34980A Read Counter Data.vi"/>
		<Item Name="AG34980A Read Counter Frequency.vi" Type="VI" URL="../Public/AG34980A Read Counter Frequency.vi"/>
		<Item Name="AG34980A Read Counter Period.vi" Type="VI" URL="../Public/AG34980A Read Counter Period.vi"/>
		<Item Name="AG34980A Read Counter Pulse Duty Cycle.vi" Type="VI" URL="../Public/AG34980A Read Counter Pulse Duty Cycle.vi"/>
		<Item Name="AG34980A Read Counter Pulse Width.vi" Type="VI" URL="../Public/AG34980A Read Counter Pulse Width.vi"/>
		<Item Name="AG34980A Read Selected Digital Input Memory Data.vi" Type="VI" URL="../Public/AG34980A Read Selected Digital Input Memory Data.vi"/>
		<Item Name="AG34980A Read Totalizer Data.vi" Type="VI" URL="../Public/AG34980A Read Totalizer Data.vi"/>
		<Item Name="AG34980A Read.vi" Type="VI" URL="../Public/AG34980A Read.vi"/>
		<Item Name="AG34980A Reading Count.vi" Type="VI" URL="../Public/AG34980A Reading Count.vi"/>
		<Item Name="AG34980A Relay Cycles.vi" Type="VI" URL="../Public/AG34980A Relay Cycles.vi"/>
		<Item Name="AG34980A Remove Readings.vi" Type="VI" URL="../Public/AG34980A Remove Readings.vi"/>
		<Item Name="AG34980A Reset All Microwave Modules.vi" Type="VI" URL="../Public/AG34980A Reset All Microwave Modules.vi"/>
		<Item Name="AG34980A Reset.vi" Type="VI" URL="../Public/AG34980A Reset.vi"/>
		<Item Name="AG34980A Scan on Alarm.vi" Type="VI" URL="../Public/AG34980A Scan on Alarm.vi"/>
		<Item Name="AG34980A Scan Status.vi" Type="VI" URL="../Public/AG34980A Scan Status.vi"/>
		<Item Name="AG34980A Self-Test.vi" Type="VI" URL="../Public/AG34980A Self-Test.vi"/>
		<Item Name="AG34980A Send Software Trigger.vi" Type="VI" URL="../Public/AG34980A Send Software Trigger.vi"/>
		<Item Name="AG34980A Sequence Catalog.vi" Type="VI" URL="../Public/AG34980A Sequence Catalog.vi"/>
		<Item Name="AG34980A Sequence Trigger Source.vi" Type="VI" URL="../Public/AG34980A Sequence Trigger Source.vi"/>
		<Item Name="AG34980A Single Step Digital Input Memory.vi" Type="VI" URL="../Public/AG34980A Single Step Digital Input Memory.vi"/>
		<Item Name="AG34980A Single Step Digital Output Memory.vi" Type="VI" URL="../Public/AG34980A Single Step Digital Output Memory.vi"/>
		<Item Name="AG34980A Start Digital Input Memory.vi" Type="VI" URL="../Public/AG34980A Start Digital Input Memory.vi"/>
		<Item Name="AG34980A Start Digital Output Memory.vi" Type="VI" URL="../Public/AG34980A Start Digital Output Memory.vi"/>
		<Item Name="AG34980A Statistics.vi" Type="VI" URL="../Public/AG34980A Statistics.vi"/>
		<Item Name="AG34980A Stop Digital Input Memory.vi" Type="VI" URL="../Public/AG34980A Stop Digital Input Memory.vi"/>
		<Item Name="AG34980A Stop Digital Output Memory.vi" Type="VI" URL="../Public/AG34980A Stop Digital Output Memory.vi"/>
		<Item Name="AG34980A Switch.vi" Type="VI" URL="../Public/AG34980A Switch.vi"/>
		<Item Name="AG34980A Temperature Auto Zero.vi" Type="VI" URL="../Public/AG34980A Temperature Auto Zero.vi"/>
		<Item Name="AG34980A Thermocouple Check.vi" Type="VI" URL="../Public/AG34980A Thermocouple Check.vi"/>
		<Item Name="AG34980A Time.vi" Type="VI" URL="../Public/AG34980A Time.vi"/>
		<Item Name="AG34980A Totalizer.vi" Type="VI" URL="../Public/AG34980A Totalizer.vi"/>
		<Item Name="AG34980A Trigger Sequence Immediate.vi" Type="VI" URL="../Public/AG34980A Trigger Sequence Immediate.vi"/>
		<Item Name="CheckOper_45.vi" Type="VI" URL="../Public/CheckOper_45.vi"/>
		<Item Name="DAC_example.vi" Type="VI" URL="../Public/DAC_example.vi"/>
		<Item Name="DigIo_example.vi" Type="VI" URL="../Public/DigIo_example.vi"/>
		<Item Name="ExampleReadme.txt" Type="Document" URL="../Public/ExampleReadme.txt"/>
		<Item Name="MUX_example.vi" Type="VI" URL="../Public/MUX_example.vi"/>
	</Item>
	<Item Name="Examples" Type="Folder"/>
	<Item Name="Private" Type="Folder"/>
</Library>
